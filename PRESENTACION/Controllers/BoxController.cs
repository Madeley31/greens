﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ENTIDAD;
using BL;
using System.Drawing;
using System.Web.UI.WebControls;
using System.IO;

namespace PRESENTACION.Controllers
{
    public class BoxController : Controller
    {
        // GET: Box
        BoxBL boxBL = new BoxBL();
        public ActionResult ListadoBox()
        {
            return View(boxBL.ListadoBox());
        }

        [HttpGet]
        public ActionResult ActualizarBox()
        {

            return View();
        }
        [HttpPost]
        public ActionResult ActualizarBox(string txtIdBox, string txtNombreBox, string imagen)
        {
            BoxE boxE = new BoxE();
            boxE.idBox = Convert.ToInt32(txtIdBox);
            boxE.nombreBox = txtNombreBox;

            string path = Server.MapPath("~/Images/" + imagen);
            byte[] byteImagen = StreamFile(path);

            boxE.imagen = byteImagen;

            if (boxBL.ActualizarBox(boxE)) return Content("<script>alert('Actualizado correctamente')</script>");
            else return Content("<script>alert('Error!')</script>");

        }

        private byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

    }
}