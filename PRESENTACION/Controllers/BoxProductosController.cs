﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ENTIDAD;
using BL;
using System.Data;

namespace PRESENTACION.Controllers
{
    public class BoxProductosController : Controller
    {
        // GET: BoxProductos

        BoxProductosBL boxProductosBL = new BoxProductosBL();
        BoxBL boxBL = new BoxBL();

        public ActionResult Index()
        {
            List<BoxProductosE> lstBoxProductos = boxProductosBL.ListadoBox();

            List<BoxE> lstBox = boxBL.ListadoBox();

            ViewBag.ListadoBoxProducto = lstBoxProductos;
            ViewBag.ListadoBox = lstBox;

            return View();
        }


    }
}