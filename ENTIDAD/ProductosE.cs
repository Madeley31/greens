﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
   public class ProductosE
    {
        public int idProducto { get; set; }
        public string nombreProducto { get; set; }

        public decimal precio { get; set; }

        public int cantidad { get; set; }
    }
}
