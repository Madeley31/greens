﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
    public class BoxProductosE
    {
        public int idBoxProducto { get; set; }
        public BoxE idBox = new BoxE();
        public ProductosE idProducto = new ProductosE();
        public int cantidad { get; set; }
        public decimal total { get; set; }
        
        public byte[]  imagen { get; set; } 
    }
}
