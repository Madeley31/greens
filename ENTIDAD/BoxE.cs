﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
    public class BoxE
    {
        public int idBox { get; set; }
        public string nombreBox { get; set; }
        public byte[] imagen { get; set; }

    }
}
