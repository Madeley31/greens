﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using DAL;
using System.Data;

namespace BL
{
    public class BoxBL
    {
        BoxDAL boxDal = new BoxDAL();

        public bool ActualizarBox(BoxE boxE)
        {
            return boxDal.ActualizarBox(boxE);
        }
        public List<BoxE> ListadoBox()
        {
            return boxDal.ListadoBox();
        }
        public List<BoxE> BuscarBox(int idBox)
        {
            return boxDal.BuscarBox(idBox);
        }
    }
}
