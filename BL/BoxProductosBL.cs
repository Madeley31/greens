﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using DAL;
using System.Data;

namespace BL
{
    public class BoxProductosBL
    {
        BoxProductosDAL boxProductosDAL = new BoxProductosDAL();

        public List<BoxProductosE> ListadoBox()
        {
            return boxProductosDAL.ListadoBox();
        }

    }
}
