﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.ComponentModel;

namespace DAL
{
   public  class BoxDAL
    {
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["conexion"].ToString());
        SqlCommand cmd = new SqlCommand();

        public bool ActualizarBox(BoxE boxE)
        {
            bool actualizar = false;
            try
            {
                cmd = new SqlCommand("spBox_Actualizar", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@idBox", boxE.idBox);
                cmd.Parameters.AddWithValue("@nombreBox", boxE.nombreBox);
                cmd.Parameters.AddWithValue("@imagen", boxE.imagen);

                cn.Open();

                int i = (int)cmd.ExecuteNonQuery();
                if (i == 1) actualizar = true;
                else actualizar = false;
                cn.Close();


            }
            catch (Exception e)
            {

                return false;
                throw;
            }

            finally{ cn.Close(); }

            return actualizar;

        }

        public List<BoxE> ListadoBox()
        {
            List<BoxE> lst = new List<BoxE>();
            try
            {
                cmd = new SqlCommand("spBox_Listar", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    BoxE boxE = new BoxE();
                    boxE.idBox = Convert.ToInt32(dr[0].ToString());
                    boxE.nombreBox = dr[1].ToString();
                    boxE.imagen =(byte[]) dr[2];
                    lst.Add(boxE);
                }
                cn.Close();

            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                throw;
            }

            finally { cn.Close(); }

            return lst;

        }
        public List<BoxE> BuscarBox(int idBox)
        {
            List<BoxE> lst = new List<BoxE>();
            try
            {
                cmd = new SqlCommand("spBox_Buscar", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idBox", idBox);

                cn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    BoxE boxE = new BoxE();
                    boxE.idBox = Convert.ToInt32(dr[0].ToString());
                    boxE.nombreBox = dr[1].ToString();
                    boxE.imagen = (byte[])dr[2];
                    lst.Add(boxE);
                }
                cn.Open();

            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                throw;
            }

            finally { cn.Close(); }

            return lst;

        }
    }
}
