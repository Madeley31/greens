﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.ComponentModel;
using System.CodeDom;

namespace DAL

{



    public class BoxProductosDAL
    {

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["conexion"].ToString());
        SqlCommand cmd = new SqlCommand();

        public List<BoxProductosE> ListadoBox()
        {
            List<BoxProductosE> lst = new List<BoxProductosE>(); 
            try
            {
                cmd = new SqlCommand("spBoxProducto_Listar", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    BoxProductosE boxProductosE = new BoxProductosE();
                    boxProductosE.idBoxProducto = Convert.ToInt32(dr[0].ToString());
                    boxProductosE.idBox.nombreBox = dr[1].ToString();
                    boxProductosE.idProducto.nombreProducto = dr[2].ToString();
                    boxProductosE.cantidad = Convert.ToInt32(dr[3].ToString());
                    boxProductosE.idProducto.precio = Convert.ToDecimal(dr[4].ToString());
                    boxProductosE.total = Convert.ToDecimal(Convert.ToInt32(dr[3].ToString()) * Convert.ToDecimal(dr[4].ToString()));
                    boxProductosE.imagen = (byte[])dr[6] ;
                    lst.Add(boxProductosE);
                    

                }
                cn.Close();
                return lst;

            }
            catch (Exception E)
            {

                throw;
            }
            finally
            {
                cn.Close();
            }
        }

    }
}
